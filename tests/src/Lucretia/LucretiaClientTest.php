<?php

namespace Swigle\Lucretia\Api;

use GuzzleHttp\ClientInterface as HttpClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Swigle\Lucretia\CommandInterface;
use Swigle\Lucretia\Exception\InvalidCommandException;
use Swigle\Lucretia\Exception\RequestException;
use Swigle\Lucretia\LucretiaClient;
use TypeError;


/**
 * Class LucretiaClientTest
 *
 * @package Swigle\Lucretia\Api
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 13/09/2018 12:46
 * @covers \Swigle\Lucretia\LucretiaClient
 */
class LucretiaClientTest extends TestCase
{
    /**
     * @var LucretiaClient|MockObject
     */
    private $client;

    /**
     * @var HttpClientInterface|MockObject
     */
    private $mockHttpClient;

    protected function setUp()//:void
    {
        parent::setUp();

        $this->mockHttpClient = $this->createHttpClientMock();

        /* @var $client LucretiaClient|MockObject */
        $this->client = $this->getMockForAbstractClass(LucretiaClient::class, [
            [
                'endpoint' => 'http://dev.rvb.beta7.swigledev.nl/lucretia',
                'token' => 'test',

                'company' => 'solarclarity',
                'environment' => 'test-environment',
            ],
            $this->mockHttpClient
        ]);
    }

    public function testExecuteWithNoMethod()//:void
    {
        $mockCommand = $this->createCommandMock();

        $mockCommand->expects($this->once())->method('hasMethod')->willReturn(false);

        $this->expectException(InvalidCommandException::class);
        $this->expectExceptionMessage('The given command must have a method');

        try {
            $this->client->execute($mockCommand);
        } catch (GuzzleException $ex) {
            $this->fail(get_class($ex).' - '.$ex->getMessage());
        }

        $this->assertEquals(400, $this->client->getStatusCode());
    }

    public function testExecuteOnNonExistingRoute()//:void
    {
        $mockResponse = $this->createResponseMock(400, json_encode([
            'name' => 'Bad Request',
            'message' => 'This API doesn\'t exist',
            'code' => 0,
            'status' => 400,
            'type' => 'yii\\web\\BadRequestHttpException',
        ]));

        $mockClientException = new ClientException('00ps', $this->createRequestMock(), $mockResponse);

        $mockCommand = $this->createCommandMock();
        $mockCommand->expects($this->once())->method('hasMethod')->willReturn(true);
        $mockCommand->expects($this->once())->method('getMethod');
        $mockCommand->expects($this->once())->method('getUri');
        $mockCommand->expects($this->once())->method('getOptions');

        $this->mockHttpClient->expects($this->once())->method('request')
            ->withAnyParameters()
            ->willThrowException($mockClientException)
        ;

        $this->expectException(RequestException::class);
        $this->expectExceptionMessage('Client error - This API doesn\'t exist');

        try {
            $this->client->execute($mockCommand);
        } catch (GuzzleException $ex) {
            $this->fail(get_class($ex).' - '.$ex->getMessage());
        }

        $this->assertEquals(400, $this->client->getStatusCode());
    }

    public function testGetCommand()//:void
    {
        $query = $options = [];
        $actualCommand = $this->client->getCommand('test', $query, $options);

        $uri = $actualCommand->getUri();
        $this->assertInstanceOf(Uri::class, $uri);
        $this->assertEquals('http://dev.rvb.beta7.swigledev.nl/lucretia/api/solarclarity/test-environment/test', $uri->__toString());
        $subset = [
            'headers' => [
                'Accept'        => 'application/json',
                'Content-Type'  => 'application/json',
                'Authorization' => 'Bearer test',
            ]
        ];
        $this->assertArraySubset($subset, $actualCommand->getOptions());

        $this->expectException(TypeError::class);
        $this->expectExceptionMessage('Return value of Swigle\Lucretia\Command::getMethod() must be of the type string, null returned');
        $actualCommand->getMethod();
    }

    public function testGetCommandWithQueryString()//:void
    {
        $options = [];
        $query = [
            'per-page' => 25,
            'page' => 5,
        ];
        $actualCommand = $this->client->getCommand('test', $query, $options);

        $uri = $actualCommand->getUri();
        $this->assertInstanceOf(Uri::class, $uri);
        $this->assertEquals(
            'http://dev.rvb.beta7.swigledev.nl/lucretia/api/solarclarity/test-environment/test?per-page=25&page=5',
            $uri->__toString()
        );
        $subset = [
            'headers' => [
                'Accept'        => 'application/json',
                'Content-Type'  => 'application/json',
                'Authorization' => 'Bearer test',
            ]
        ];
        $this->assertArraySubset($subset, $actualCommand->getOptions());

        $this->expectException(TypeError::class);
        $this->expectExceptionMessage('Return value of Swigle\Lucretia\Command::getMethod() must be of the type string, null returned');
        $actualCommand->getMethod();
    }

    /**
     * @return HttpClientInterface|MockObject
     */
    private function createHttpClientMock()
    {
        $apiClientMockBuilder = $this->getMockBuilder(HttpClientInterface::class)
            ->setMockClassName('MockHttpClientInterface')
        ;

        return $apiClientMockBuilder->getMock();
    }

    /**
     * @param int $statusCode
     * @param string $body
     * @return ResponseInterface|MockObject
     */
    private function createResponseMock(int $statusCode = 200, string $body = '')
    {
        $responseMock = $this->getMockBuilder(ResponseInterface::class)
            ->setMockClassName('MockResponseInterface')
            ->getMock()
        ;

        $responseMock->expects($this->atLeastOnce())->method('getStatusCode')->willReturn($statusCode);
        $responseMock->expects($this->atLeastOnce())->method('getBody')->willReturn($body);

        return $responseMock;
    }

    /**
     * @return RequestInterface|MockObject
     */
    private function createRequestMock()
    {
        $responseMockBuilder = $this->getMockBuilder(RequestInterface::class)
            ->setMockClassName('MockRequestInterface')
        ;

        return $responseMockBuilder->getMock();
    }

    /**
     * @return CommandInterface|MockObject
     */
    private function createCommandMock()
    {
        $responseMockBuilder = $this->getMockBuilder(CommandInterface::class)
            ->setMockClassName('MockCommandInterface')
        ;

        return $responseMockBuilder->getMock();
    }
}
