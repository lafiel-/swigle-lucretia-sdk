<?php

namespace Swigle\Lucretia;

use PHPUnit\Framework\TestCase;

/**
 * Class UtilsTest
 * s
 * @package Swigle\Lucretia
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 11/09/2018 13:45
 * @covers \Swigle\Lucretia\Utils
 */
class UtilsTest extends TestCase
{
    /**
     * @param $data
     * @param $expectedResult
     * @dataProvider providerDescribeType
     */
    public function testDescribeType($data, $expectedResult)
    {
        $actualResult = Utils::describeType($data);

        $this->assertEquals($actualResult, $expectedResult);
    }

    public function providerDescribeType()
    {
        $object = new \stdClass();
        $object->prop = 'yohoho';

        return [
            [0.0,               'float(0)'],
            [1,                 'int(1)'],
            ['yohoho',          'string(6) "yohoho"'],
            [['array'],         'array(1)'],
            [$object,           'object(stdClass)'],
        ];
    }
}
