<?php

namespace Swigle\Lucretia\Passthrough;

use PHPUnit\Framework\TestCase;
use stdClass;
use Swigle\Lucretia\Sdk;

/**
 * Class PassthroughClientTest
 *
 * @package Swigle\Lucretia
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 12/09/2017 09:03
 * @covers \Swigle\Lucretia\Passthrough\PassthroughClient
 */
class PassthroughClientTest extends TestCase
{
    /**
     * @var PassthroughClient
     */
    private $client;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $sdk = new Sdk();

        $this->client = $sdk->createPassthrough([
            'token' => 'test',
            'company' => 'solarclarity',
            'environment' => 'test-environment',

            'endpoint' => 'http://dev.rvb.beta7.swigledev.nl/lucretia',
        ]);
    }

    public function testPostPassthrough()
    {
        $query = [
            'skip' => 0,
            'take' => 1000,
        ];
        
        $data = [
        "FbSales" => [
            "Element" => [
            [
                "Fields" => [
                    "DbId" => "11894",
                    "CuId" => "EUR",
                    "War" => "*****",
                    "DaDe" => "2017-11-22",
                    "RfCs" => "2WS",
                ],
                "Objects" => [
                    "FbSalesLines" => [
                        [
                            "Element" => [
                                [
                                    "Fields" => [
                                        "VaIt" => "Art",
                                        "ItCd" => "210026",
                                        "BiUn" => "Stk",
                                        "QuUn" => 15
                                    ]
                                ],
                                [
                                    "Fields" => [
                                        "VaIt" => "Art",
                                        "ItCd" => "210026",
                                        "BiUn" => "Stk",
                                        "QuUn" => 15,
                                    ]
                                ]
                            ]
                        ],
                    ]
                ]
            ]
        ]
    ]
];

        $response = $this->client->post('connectors/FbSales', $query, $data);

        $this->assertEquals(201, $this->client->getStatusCode());
        $this->assertNotEmpty($response);
        $this->assertInstanceOf(stdClass::class, $response);
    }

//    /**
//     * @covers \Swigle\Lucretia\SDK\Client
//     */
//    public function testGetPassthroughRPC()
//    {
//        $client = new Client([
//            'token' => 'test',
//            'company' => 'solarclarity',
//            'environment' => 'test-environment'
//        ]);
//
//        $query = [
//            'skip' => 0,
//            'take' => 1000,
//        ];
//
//        $response = $client->passthrough('get', 'connectors/XS_Verkooporder', $query);
//
//        $this->assertNotEmpty($response);
//    }
}
