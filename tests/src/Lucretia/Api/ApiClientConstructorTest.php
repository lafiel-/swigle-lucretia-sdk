<?php

namespace Swigle\Lucretia\Api;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\ClientInterface as HttpClientInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ApiClientConstructorTest
 *
 * @package Swigle\Lucretia\Api
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 13/09/2018 14:01
 * @covers \Swigle\Lucretia\Api\ApiClient
 * @covers \Swigle\Lucretia\LucretiaClient
 */
class ApiClientConstructorTest extends TestCase
{
    /**
     * @var ApiClient
     */
    private $client;

    /**
     * @throws GuzzleException
     * @void
     */
    public function testConstructor()//: void
    {
        $mockResponse = $this->createResponseMock(400, json_encode([
            'name' => 'Bad Request',
            'message' => 'This API doesn\'t exist',
            'code' => 0,
            'status' => 400,
            'type' => 'yii\\web\\BadRequestHttpException',
        ]));

        $mockResponse->expects($this->once())->method('getHeader')
            ->with('Allow')
            ->willReturn(['GET , POST'])
        ;

        $mockHttpClient = $this->createHttpClientMock();

        $mockHttpClient->expects($this->once())->method('request')
            ->withAnyParameters()
            ->willReturn($mockResponse)
        ;

        $this->client = new ApiClient([
            'endpoint' => 'http://dev.rvb.beta7.swigledev.nl/lucretia',
            'token' => 'test',

            'company' => 'solarclarity',
            'environment' => 'test-environment',
            'api' => 'debtors'
        ], $mockHttpClient);
    }

    /**
     * @return HttpClientInterface|MockObject
     */
    private function createHttpClientMock()
    {
        $apiClientMockBuilder = $this->getMockBuilder(HttpClientInterface::class)
            ->setMockClassName('MockGuzzleHttpClient')
        ;

        return $apiClientMockBuilder->getMock();
    }

    /**
     * @param int $statusCode
     * @param string $body
     * @return ResponseInterface|MockObject
     */
    private function createResponseMock(int $statusCode = 200, string $body = '')
    {
        $responseMockBuilder = $this->getMockBuilder(ResponseInterface::class)
            ->setMockClassName('MockResponse')
        ;

        return $responseMockBuilder->getMock();
    }
}
