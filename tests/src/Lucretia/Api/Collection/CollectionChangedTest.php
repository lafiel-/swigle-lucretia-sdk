<?php

namespace Swigle\Lucretia\Api\Collection;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use stdClass;
use Swigle\Lucretia\Api\ApiClientInterface;
use Swigle\Lucretia\CommandInterface;
use Swigle\Lucretia\Exception\CanNotSetPerPageException;
use Swigle\Lucretia\Exception\CollectionHasChangedException;

/**
 * Class CollectionChangedTest
 *
 * @package Swigle\Lucretia\Api
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 12/09/2018 10:20
 * @covers \Swigle\Lucretia\Api\Collection\Collection
 */
class CollectionChangedTest extends TestCase
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var CommandInterface|MockObject
     */
    private $mockApiClient;

    /**
     * @inheritdoc
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function setUp()
    {
        $mockApiName = 'php-unit-api';
        $this->mockApiClient = $this->createApiClientMock();
        $mockCommand = $this->createApiClientCommandMock();
        $mockResponse = $this->createApiClientResponseMock();
        $mockQuery1 = [
            'per-page' => 20,
            'page' => 1,
        ];
        $mockQuery2 = [
            'per-page' => 20,
            'page' => 1,
        ];
        $mockQuery3 = [
            'per-page' => 1,
            'page' => 1,
        ];
        $mockQuery4 = [
            'per-page' => 1,
            'page' => 2,
        ];

        $this->mockApiClient->expects($this->atLeast(0))->method('getCommand')
            ->withConsecutive(
                [$mockApiName, $mockQuery1],
                [$mockApiName, $mockQuery2],
                [$mockApiName, $mockQuery3],
                [$mockApiName, $mockQuery4]
            )
            ->willReturn($mockCommand)
        ;
        $this->mockApiClient->expects($this->atLeast(3))->method('execute')->with($mockCommand)->willReturn($mockResponse);
        $this->mockApiClient->expects($this->atLeast(3))->method('getApi')->willReturn($mockApiName);

        $this->collection = new Collection($this->mockApiClient);
        $this->collection->setPerPage(1);
    }

    public function testChangingDataSet()
    {
        $this->expectException(CollectionHasChangedException::class);
        $this->expectExceptionMessage('The collection has changed, you must restart your iteration for "php-unit-api"');

        $this->collection->next();
    }

    /**
     * @return ApiClientInterface|MockObject
     */
    private function createApiClientMock()
    {
        $apiClientMockBuilder = $this->getMockBuilder(ApiClientInterface::class);

        return $apiClientMockBuilder->getMock();
    }

    /**
     * @return CommandInterface|MockObject
     */
    private function createApiClientCommandMock()
    {
        $commandMockBuilder = $this->getMockBuilder(CommandInterface::class);

        $mock = $commandMockBuilder->getMock();

        $mock->expects($this->atLeastOnce())->method('setMethod')->withAnyParameters();

        return $mock;
    }

    /**
     * @return CommandInterface|MockObject
     */
    private function createApiClientResponseMock()
    {
        $responseMockBuilder = $this->getMockBuilder(ResponseInterface::class);

        $mock = $responseMockBuilder->getMock();

        $mock->expects($this->any())->method('getHeader')->willReturnCallback(function () {
            global $counter;

            // set the counter to zero if it's new (null)
            if ($counter === null) {
                $counter = 0;
            }

            $args = func_get_args();
            switch ($args[0]) {
                case 'X-Pagination-Total-Count':
                    $value = [10];
                    break;
                case 'X-Pagination-Current-Page':
                    global $currentPage;
                    if ($currentPage === null) {
                        $counter = 1;
                    }
                    $value = [$currentPage++];
                    break;
                case 'X-Lucretia-Data-Set-Hash':
                    if ($counter <= 3) {
                        $value = ['Yohoho'];
                    } else {
                        $value = ['Yohoho-has-changed'];
                    }
                    break;
                default:
                    $value = '?';
                    break;
            }

            $counter++;

            return $value;
        });

        $mock->expects($this->exactly(2))->method('getBody')->willReturnCallback(function () {
            global $counter;
            if ($counter <= 3) {
                $value = $this->getMockData(10);
            } else {
                $value = $this->getMockData(1);
            }
            return $value;
        });

        return $mock;
    }

    private function getMockData(int $count): string
    {
        $obj = new stdClass();
        $obj->prop = 1;

        $rawData = array_fill(0, $count, $obj);
        array_walk($rawData, function (stdClass &$obj, $key) {
            $obj = clone $obj;
            $obj->prop = $key;
        });

        return json_encode($rawData);
    }
}
