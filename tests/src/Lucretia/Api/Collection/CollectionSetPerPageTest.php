<?php

namespace Swigle\Lucretia\Api\Collection;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use stdClass;
use Swigle\Lucretia\Api\ApiClientInterface;
use Swigle\Lucretia\CommandInterface;
use Swigle\Lucretia\Exception\CanNotSetPerPageException;

/**
 * Class CollectionSetPerPageTest
 *
 * @package Swigle\Lucretia\Api
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 12/09/2018 10:20
 * @covers \Swigle\Lucretia\Api\Collection\Collection
 */
class CollectionSetPerPageTest extends TestCase
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var CommandInterface|MockObject
     */
    private $mockApiClient;

    /**
     * @inheritdoc
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function setUp()
    {
        $mockApiName = 'php-unit-api';
        $this->mockApiClient = $this->createApiClientMock();
        $mockCommand = $this->createApiClientCommandMock();
        $mockResponse = $this->createApiClientResponseMock();
        $mockQuery1 = [
            'per-page' => 20,
            'page' => 1,
        ];
        $mockQuery2 = [
            'per-page' => 2,
            'page' => 1,
        ];

        $this->mockApiClient->expects($this->exactly(4))->method('getCommand')
            ->withConsecutive([$mockApiName, $mockQuery1], [$mockApiName, $mockQuery1], [$mockApiName, $mockQuery2])
            ->willReturn($mockCommand)
        ;
        $this->mockApiClient->expects($this->exactly(4))->method('execute')->with($mockCommand)->willReturn($mockResponse);
        $this->mockApiClient->expects($this->exactly(4))->method('getApi')->willReturn($mockApiName);

        $this->collection = new Collection($this->mockApiClient);
    }

    public function testSetPerPage()
    {
        $this->collection->setPerPage(2);

        $this->expectException(CanNotSetPerPageException::class);
        $this->expectExceptionMessage('It\'s not possible to change the items per page while the iterator is in use');

        $this->collection->next();
        $this->collection->setPerPage(2);
    }

    /**
     * @return ApiClientInterface|MockObject
     */
    private function createApiClientMock()
    {
        $apiClientMockBuilder = $this->getMockBuilder(ApiClientInterface::class);

        return $apiClientMockBuilder->getMock();
    }

    /**
     * @return CommandInterface|MockObject
     */
    private function createApiClientCommandMock()
    {
        $commandMockBuilder = $this->getMockBuilder(CommandInterface::class);

        $mock = $commandMockBuilder->getMock();

        $mock->expects($this->atLeastOnce())->method('setMethod')->withAnyParameters();

        return $mock;
    }

    /**
     * @return CommandInterface|MockObject
     */
    private function createApiClientResponseMock()
    {
        $responseMockBuilder = $this->getMockBuilder(ResponseInterface::class);

        $mock = $responseMockBuilder->getMock();

        $mock->expects($this->any())->method('getHeader')->willReturnCallback(function () {
            $args = func_get_args();
            switch ($args[0]) {
                case 'X-Pagination-Total-Count':
                    $value = [10];
                    break;
                case 'X-Pagination-Current-Page':
                    $value = [2];
                    break;
                case 'X-Lucretia-Data-Set-Hash':
                    $value = ['Yohoho'];
                    break;
                default:
                    $value = '?';
                    break;
            }
            return $value;
        });

        $mock->expects($this->any())->method('getBody')->willReturn($this->getMockData());

        return $mock;
    }

    private function getMockData(): string
    {
        $obj = new stdClass();
        $obj->prop = 1;

        $rawData = array_fill(0, 10, $obj);
        array_walk($rawData, function (stdClass &$obj, $key) {
            $obj = clone $obj;
            $obj->prop = $key;
        });

        return json_encode($rawData);
    }
}
