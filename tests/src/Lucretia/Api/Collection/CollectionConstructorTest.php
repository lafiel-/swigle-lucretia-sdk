<?php

namespace Swigle\Lucretia\Api\Collection;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use stdClass;
use Swigle\Lucretia\Api\ApiClientInterface;
use Swigle\Lucretia\CommandInterface;

/**
 * Class CollectionConstructorTest
 *
 * @package Swigle\Lucretia\Api
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 12/09/2018 10:20
 * @covers \Swigle\Lucretia\Api\Collection\Collection
 */
class CollectionConstructorTest extends TestCase
{
    public function testConstructor()
    {
        $mockApiName = 'php-unit-api';
        $apiClient = $this->createApiClientMock();
        $mockCommand = $this->createApiClientCommandMock();
        $mockResponse = $this->createApiClientResponseMock();
        $mockQuery = [
            'per-page' => 20,
            'page' => 1,
        ];

        $apiClient->expects($this->exactly(2))->method('getCommand')->with($mockApiName, $mockQuery)->willReturn($mockCommand);
        $apiClient->expects($this->exactly(2))->method('execute')->with($mockCommand)->willReturn($mockResponse);
        $apiClient->expects($this->exactly(2))->method('getApi')->willReturn($mockApiName);

        $collection = new Collection($apiClient);
    }

    /**
     * @return ApiClientInterface|MockObject
     */
    private function createApiClientMock()
    {
        $apiClientMockBuilder = $this->getMockBuilder(ApiClientInterface::class);

        return $apiClientMockBuilder->getMock();
    }

    /**
     * @return CommandInterface|MockObject
     */
    private function createApiClientCommandMock()
    {
        $commandMockBuilder = $this->getMockBuilder(CommandInterface::class);

        $mock = $commandMockBuilder->getMock();

        $mock->expects($this->atLeastOnce())->method('setMethod')->withAnyParameters();

        return $mock;
    }

    /**
     * @return CommandInterface|MockObject
     */
    private function createApiClientResponseMock()
    {
        $responseMockBuilder = $this->getMockBuilder(ResponseInterface::class);

        $mock = $responseMockBuilder->getMock();

        $mock->expects($this->any())->method('getHeader')->willReturnCallback(function () {
            $args = func_get_args();
            switch ($args[0]) {
                case 'X-Pagination-Total-Count':
                    $value = [10];
                    break;
                case 'X-Pagination-Current-Page':
                    $value = [2];
                    break;
                case 'X-Lucretia-Data-Set-Hash':
                    $value = ['Yohoho'];
                    break;
                default:
                    $value = '?';
                    break;
            }
            return $value;
        });

        $mock->expects($this->any())->method('getBody')->willReturn($this->getMockData());

        return $mock;
    }

    private function getMockData(): string
    {
        $obj = new stdClass();
        $obj->prop = 1;

        $rawData = array_fill(0, 10, $obj);
        array_walk($rawData, function (stdClass &$obj, $key) {
            $obj = clone $obj;
            $obj->prop = $key;
        });

        return json_encode($rawData);
    }
}
