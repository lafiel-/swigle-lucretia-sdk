<?php

namespace Swigle\Lucretia\Api;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * Class ArgumentsResolverTest
 *
 * @package Swigle\Lucretia
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 12/09/2017 09:03
 * @covers \Swigle\Lucretia\Api\ArgumentsResolver
 */
class ArgumentsResolverTest extends TestCase
{
    /**
     * @var ArgumentsResolver
     */
    private $resolver;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->resolver = new ArgumentsResolver();
    }

    /**
     * @param string $method
     * @param array $args
     * @param mixed $expected
     * @param string|null $expectedException
     * @dataProvider providerCallAllowedMethod
     */
    public function testCallAllowedMethod(string $method, array $args, $expected, string $expectedException = null)
    {
        if ($expectedException !== null) {
            $this->expectException($expectedException);
            $this->expectExceptionMessage($expected);
        }

        $resolvedArgs = $this->resolver->resolve($method, $args);
        $this->assertArraySubset($expected, $resolvedArgs);
    }

    public function providerCallAllowedMethod()
    {
        return [
            [   'get', [],              "Missing required call argument: \n\nid: (int)\n\n  The id of the entity to get", InvalidArgumentException::class],
            [   'get', [1],             ['id' => 1] ],
            [   'get', ['bad-arg'],     "Invalid configuration value provided for \"id\". Expected int, but got string(7) \"bad-arg\"\n\nid: (int)\n\n  The id of the entity to get", InvalidArgumentException::class],

            [   'post', [],           "Missing required call argument: \n\ndata: (array)\n\n  This is the entity data", InvalidArgumentException::class],
            [   'post', [[]],            ['data' => []] ],
            [   'post', ['bad-arg'],     "Invalid configuration value provided for \"data\". Expected array, but got string(7) \"bad-arg\"\n\ndata: (array)\n\n  This is the entity data", InvalidArgumentException::class],

            [   'put', [],              "Missing required call argument: \n\nid: (int)\n\n  The id of the entity to put", InvalidArgumentException::class],
            [   'put', [1],             "Missing required call argument: \n\ndata: (array)\n\n  This is the entity data", InvalidArgumentException::class],
            [   'put', [1, []],         ['id' => 1, 'data' => []] ],
            [   'put', ['bad-arg'],     "Invalid configuration value provided for \"id\". Expected int, but got string(7) \"bad-arg\"\n\nid: (int)\n\n  The id of the entity to put", InvalidArgumentException::class],
            [   'put', [1, 'bad-arg'],  "Invalid configuration value provided for \"data\". Expected array, but got string(7) \"bad-arg\"\n\ndata: (array)\n\n  This is the entity data", InvalidArgumentException::class],

            [   'delete', [],           "Missing required call argument: \n\nid: (int)\n\n  The id of the entity to delete", InvalidArgumentException::class],
            [   'delete', [1],          ['id' => 1] ],
            [   'delete', ['bad-arg'],  "Invalid configuration value provided for \"id\". Expected int, but got string(7) \"bad-arg\"\n\nid: (int)\n\n  The id of the entity to delete", InvalidArgumentException::class],
        ];
    }
}
