<?php

namespace Swigle\Lucretia\Api;

use GuzzleHttp\ClientInterface as HttpClientInterface;
use function GuzzleHttp\Psr7\uri_for;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use ReflectionClass;
use ReflectionException;
use Swigle\Lucretia\Api\Collection\CollectionFactoryInterface;
use Swigle\Lucretia\Exception\NotMethodAllowedException;
use Swigle\Testing\UnitTest\Utility\UnderTestProperty;


/**
 * Class ApiClientTest
 *
 * @package Swigle\Lucretia
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 12/09/2017 09:03
 * @covers \Swigle\Lucretia\Api\ApiClient
 * @covers \Swigle\Lucretia\LucretiaClient
 */
class ApiClientTest extends TestCase
{
    use UnderTestProperty;

    /**
     * @var ApiClient
     */
    private $client;

    /**
     * @var HttpClientInterface|MockObject
     */
    private $mockHttpClient;

    /**
     * @var CollectionFactoryInterface|MockObject
     */
    private $mockCollectionFactory;

    /**
     * @inheritdoc
     * @ throws \GuzzleHttp\Exception\GuzzleException
     * @void
     */
    protected function setUp()//: void
    {
        parent::setUp();

        $this->mockHttpClient = $this->createHttpClientMock();
        $this->mockCollectionFactory = $this->createCollectionFactoryMock();

        try {

            $reflector = new ReflectionClass(ApiClient::class);
            $this->client = $reflector->newInstanceWithoutConstructor();

            $this->setUnderTestProperty($this->client, 'api', 'phpunit-api');
            $this->setUnderTestProperty($this->client, 'token', 'test-token');
            $this->setUnderTestProperty($this->client, 'endpoint', 'http://test.nl');
            $this->setUnderTestProperty($this->client, 'company', 'test-company');
            $this->setUnderTestProperty($this->client, 'environment', 'test-environment');
            $this->setUnderTestProperty($this->client, 'environment', 'test-environment');
            $this->setUnderTestProperty($this->client, 'client', $this->mockHttpClient);
            $this->setUnderTestProperty($this->client, 'collectionFactory', $this->mockCollectionFactory);
        } catch (ReflectionException $rEx) {
            $this->fail($rEx->getMessage());
        }
    }

    public function testGetApi()//: void
    {
        $this->mockHttpClient->expects($this->never())->method($this->anything());
        $this->mockCollectionFactory->expects($this->never())->method($this->anything());

        $this->assertEquals('phpunit-api', $this->client->getApi());
    }

    public function testCollection()//: void
    {
        $this->mockCollectionFactory->expects($this->once())->method('create')->with($this->client);

        $this->client->collection();
    }

    public function testCallNotAllowedMethod()
    {
        $this->mockHttpClient->expects($this->never())->method($this->anything());
        $this->mockCollectionFactory->expects($this->never())->method($this->anything());

        $this->expectException(NotMethodAllowedException::class);
        $this->expectExceptionMessage('API "phpunit-api" does not support Method "get"');

        $this->client->get(1);
    }

    public function testCall()
    {
        $responseMock = $this->createResponseMock(200, json_encode([
            'id' => 9485,
        ]));

        $this->mockHttpClient->expects($this->once())->method('request')
            ->with('GET', uri_for('http://test.nl/api/test-company/test-environment/phpunit-api/1'), [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer test-token',
                    'User-Agent' => null,
                ],
            ])
            ->willReturn($responseMock)
        ;

        $this->setUnderTestProperty($this->client, 'methods', ['get']);

        $this->client->get(1);
    }

    public function testCallWithData()
    {
        $responseMock = $this->createResponseMock(200, json_encode([
            'id' => 9485,
        ]));

        $this->mockHttpClient->expects($this->once())->method('request')
            ->with('POST', uri_for('http://test.nl/api/test-company/test-environment/phpunit-api'), [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer test-token',
                    'User-Agent' => null,
                ],
                'json' => [],
            ])
            ->willReturn($responseMock)
        ;

        $this->setUnderTestProperty($this->client, 'methods', ['post']);

        $this->client->post([]);
    }

    /**
     * @return HttpClientInterface|MockObject
     */
    private function createHttpClientMock()
    {
        $apiClientMockBuilder = $this->getMockBuilder(HttpClientInterface::class)
            ->setMockClassName('MockHttpClient')
        ;

        return $apiClientMockBuilder->getMock();
    }

    /**
     * @return CollectionFactoryInterface|MockObject
     */
    private function createCollectionFactoryMock(): CollectionFactoryInterface
    {
        $collectionFactoryMockBuilder = $this->getMockBuilder(CollectionFactoryInterface::class)
            ->setMockClassName('MockCollectionFactory')
        ;

        return $collectionFactoryMockBuilder->getMock();
    }

    /**
     * @param int $statusCode
     * @param string $body
     * @return ResponseInterface|MockObject
     */
    private function createResponseMock(int $statusCode = 200, string $body = '')
    {
        $responseMock = $this->getMockBuilder(ResponseInterface::class)
            ->setMockClassName('MockResponse')
            ->getMock()
        ;

        $responseMock->expects($this->atLeastOnce())->method('getStatusCode')->willReturn($statusCode);
        $responseMock->expects($this->atLeastOnce())->method('getBody')->willReturn($body);

        return $responseMock;
    }

    /**
     * @return RequestInterface|MockObject
     */
    private function createRequestMock()
    {
        $responseMockBuilder = $this->getMockBuilder(RequestInterface::class)
            ->setMockClassName('MockRequest')
        ;

        return $responseMockBuilder->getMock();
    }
}
