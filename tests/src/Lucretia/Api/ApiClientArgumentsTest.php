<?php

namespace Swigle\Lucretia\Api;

use PHPUnit\Framework\TestCase;

/**
 * Class ApiClientArgumentsTest
 *
 * @package Swigle\Lucretia\Api
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 13/09/2018 13:58
 * @covers \Swigle\Lucretia\Api\ApiClient
 */
class ApiClientArgumentsTest extends TestCase
{
    /**
     * @covers \Swigle\Lucretia\Api\ApiClient::getArguments
     */
    public function testGetArguments()
    {
        $this->assertArraySubset([
            'api' => [
                'type'    => 'value',
                'valid'   => ['string'],
                'doc'     => 'The lucretia API we\'re going to use',
            ],
        ], ApiClient::getArguments());
    }
}
