<?php

namespace Swigle\Lucretia\Country;

use PHPUnit\Framework\TestCase;
use Swigle\Testing\UnitTest\Utility\UnderTestProperty;

/**
 * Class CountryClientTest
 *
 * @package Swigle\Lucretia\Country
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 20/09/2018 11:49
 * @coversDefaultClass \Swigle\Lucretia\Country\CountryClient
 */
class CountryClientTest extends TestCase
{
    use UnderTestProperty;

    /**
     * @return CountryClient
     * @throws \ReflectionException
     * @covers ::__construct
     * @covers ::loadCountries
     * @covers ::getArguments
     */
    public function testConstruct()
    {
        $countryClient = new CountryClient();

        $countiesData = $this->getUnderTestProperty($countryClient, 'countries');

        $this->assertArrayHasKey('a2', $countiesData);
        $this->assertArrayHasKey('a3', $countiesData);

        return $countryClient;
    }

    /**
     * @param string $alpha2
     * @param string $expected
     * @param string|null $expectedException
     * @param CountryClient $countryClient
     * @depends testConstruct
     * @covers ::alpha2toProfit
     * @dataProvider providerAlpha2toProfit
     */
    public function testAlpha2toProfit(string $alpha2, string $expected, string $expectedException = null, CountryClient $countryClient)
    {
        if ($expectedException !== null) {
            $this->expectException($expectedException);
            $this->expectExceptionMessage($expected);
        }

        $this->assertEquals($expected, $countryClient->alpha2toProfit($alpha2));
    }

    public function providerAlpha2toProfit()
    {
        return [
            ['DE', 'D', null],
            ['Swigle', 'The Iso Alpha 2 code "Swigle" could not be translated', CountryNotFoundException::class],
        ];
    }

    /**
     * @param string $alpha3
     * @param string $expected
     * @param string|null $expectedException
     * @param CountryClient $countryClient
     * @depends testConstruct
     * @covers ::alpha3toProfit
     * @dataProvider providerAlpha3toProfit
     */
    public function testAlpha3toProfit(string $alpha3, string $expected, string $expectedException = null, CountryClient $countryClient)
    {
        if ($expectedException !== null) {
            $this->expectException($expectedException);
            $this->expectExceptionMessage($expected);
        }

        $this->assertEquals($expected, $countryClient->alpha3toProfit($alpha3));
    }

    public function providerAlpha3toProfit()
    {
        return [
            ['DEU', 'D', null],
            ['Swigle', 'The Iso Alpha 3 code "Swigle" could not be translated', CountryNotFoundException::class],
        ];
    }
}
