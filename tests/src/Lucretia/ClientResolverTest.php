<?php

namespace Swigle\Lucretia;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Swigle\Lucretia\ClientResolver
 */
class ClientResolverTest extends TestCase
{

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Missing required client configuration options
     */
    public function testEnsuresRequiredArgumentsAreProvided()
    {
        $r = new ClientResolver(ClientResolver::getDefaultArguments());
        $r->resolve([]);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid configuration value provided for "foo". Expected string, but got
     * @ expectedExceptionMessage Invalid configuration value provided for "foo". Expected string, but got int(-1)
     */
    public function testValidatesInput()
    {
        $r = new ClientResolver([
            'foo' => [
                'type'  => 'value',
                'valid' => ['string']
            ]
        ]);
        $r->resolve(['foo' => -1]);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid configuration value provided for "foo". Expected callable, but got
     * @ expectedExceptionMessage Invalid configuration value provided for "foo". Expected callable, but got string(1) "c"
     */
    public function testValidatesCallables()
    {
        $r = new ClientResolver([
            'foo' => [
                'type'   => 'value',
                'valid'  => ['callable']
            ]
        ]);
        $r->resolve(['foo' => 'c']);
    }

    public function testValidatesCallableClosure()
    {
        $r = new ClientResolver([
            'foo' => [
                'type' => 'value',
                'valid' => ['string'],
                'default' => function () {
                    return 'callable_test';
                }
            ]
        ]);
        $res = $r->resolve([]);
        $this->assertEquals('callable_test', $res['foo']);
    }

    public function checkCallable(): string
    {
        return "testcall";
    }

    public function testValidatesNotInvokeStringCallable()
    {
        $callableFunction = self::class.'::checkCallable';
        $r = new ClientResolver([
            'foo' => [
                'type'    => 'value',
                'valid'   => ['string'],
                'default' => $callableFunction
            ]
        ]);
        $res = $r->resolve([]);
        $this->assertInternalType('callable', $callableFunction);
        $this->assertEquals(
            self::class.'::checkCallable',
            $res['foo']
        );
    }
//
//    public function testCanAddConfigOptions()
//    {
//        $c = new S3Client([
//            'version'         => 'latest',
//            'region'          => 'us-west-2',
//            'bucket_endpoint' => true,
//        ]);
//        $this->assertTrue($c->getConfig('bucket_endpoint'));
//    }

    public function testSkipsNonRequiredKeys()
    {
        $r = new ClientResolver([
            'foo' => [
                'valid' => ['int'],
                'type'  => 'value'
            ]
        ]);

        $this->assertArrayHasKey('config', $r->resolve([]));
    }

    public function testAppliesUserAgent()
    {
        $r = new ClientResolver(ClientResolver::getDefaultArguments());
        $conf = $r->resolve([
            'token'         => 'test',
            'company'       => 'test',
            'environment'   => 'test',
            'user_agent' => 'PHPUnit/Unit',
        ]);
        $this->assertArrayHasKey('user_agent', $conf);
        $this->assertInternalType('array', $conf['user_agent']);
        $this->assertContains('PHPUnit/Unit', $conf['user_agent']);
        $this->assertContains('lucretia-sdk-php/' . Sdk::VERSION, $conf['user_agent']);
    }

    public function testUserAgentAlwaysStartsWithSdkAgentString()
    {
        $args = [];
        ClientResolver::_apply_user_agent([], $args);

        $this->assertStringStartsWith('lucretia-sdk-php/'.Sdk::VERSION, implode(' ', $args['user_agent']));
    }

    public function malformedEndpointProvider()
    {
        return [
            ['www.lucretia.io'], // missing protocol
            ['https://'], // missing host
        ];
    }

    /**
     * @dataProvider malformedEndpointProvider
     * @param $endpoint
     *
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Endpoints must be full URIs and include a scheme and host
     */
    public function testRejectsMalformedEndpoints($endpoint)
    {
        $args = [];
        ClientResolver::_apply_endpoint($endpoint, $args);
    }
}
