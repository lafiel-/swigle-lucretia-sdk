<?php


namespace Swigle\Lucretia;

/**
 * Class Utils
 *
 * @package Swigle\Lucretia
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 11/09/2018 12:24
 */
class Utils
{
    /**
     * Debug function used to describe the provided value type and class.
     *
     * @param mixed $input
     *
     * @return string Returns a string containing the type of the variable and if a class is provided, the class name.
     */
    public static function describeType($input): string
    {
        switch (gettype($input)) {
            case 'object':
                return 'object(' . get_class($input) . ')';
            case 'array':
                return 'array(' . count($input) . ')';
            default:
                ob_start();
                $oldValue = ini_set('xdebug.overload_var_dump', 1);
                var_dump($input);
                ini_set('xdebug.overload_var_dump', $oldValue);
                // normalize float vs double
                return str_replace('double(', 'float(', rtrim(ob_get_clean()));
        }
    }
}
