<?php

namespace Swigle\Lucretia\Api;

use InvalidArgumentException as IAE;
use Swigle\Lucretia\Utils;

/**
 * Class ArgumensResolver
 *
 * @package Swigle\Lucretia\Api
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 13/09/2018 14:52
 */
class ArgumentsResolver
{

    /** @var array Map of types to a corresponding function */
    private static $typeMap = [
        'resource' => 'is_resource',
        'callable' => 'is_callable',
        'int'      => 'is_int',
        'bool'     => 'is_bool',
        'string'   => 'is_string',
        'object'   => 'is_object',
        'array'    => 'is_array',
    ];

    /** @var array */
    private $argDefinitions = [
        'get' => [
            'id' => [
                'type' => ['int'],
                'doc' => 'The id of the entity to get',
                'required' => true,
                'position' => 0,
            ],
        ],
        'post' => [
            'data' => [
                'type' => ['array'],
                'doc' => 'This is the entity data',
                'required' => true,
                'position' => 0,
            ],
        ],
        'put' => [
            'id' => [
                'type' => ['int'],
                'doc' => 'The id of the entity to put',
                'required' => true,
                'position' => 0,
            ],
            'data' => [
                'type' => ['array'],
                'doc' => 'This is the entity data',
                'required' => true,
                'position' => 1,
            ],
        ],
        'delete' => [
            'id' => [
                'type' => ['int'],
                'doc' => 'The id of the entity to delete',
                'required' => true,
                'position' => 0,
            ],
        ],
    ];

    /**
     * Resolves ApiClient call arguments.
     * Check for missing keys in passed arguments
     *
     * @param string    $method
     * @param array     $args
     *
     * @return array Returns the array of provided options.
     * @throws IAE
     * @throws IAE
     * @see ApiClient::__call for a list of available options.
     */
    public function resolve(string $method, array $args): array
    {
        if (!isset($this->argDefinitions[$method])) {
            throw new IAE('Unknown method');
        }

        $assocArgs = [];

        foreach ($this->argDefinitions[$method] as $key => $a) {
            $position = $a['position'];
            // Add defaults, validate required values, and skip if not set.
            if (!isset($args[$position])) {
                if (empty($a['required'])) {
                    continue;
                } else {
                    $this->throwRequired($method, $args);
                }
            }

            $value = $args[$position];

            // Validate the types against the provided value.
            foreach ($a['type'] as $check) {
                if (isset(self::$typeMap[$check])) {
                    $fn = self::$typeMap[$check];
                    if ($fn($value)) {
                        goto is_valid;
                    }
                } elseif ($value instanceof $check) {
                    goto is_valid;
                }
            }

            $this->invalidType($method, $key, $value);

            // Apply the value
            is_valid:

            $assocArgs[$key] = $value;
        }

        // todo order by position?

        return $assocArgs;
    }

    /**
     * Creates a verbose error message for an invalid argument.
     *
     * @param string $method
     * @param string $name        Name of the argument that is missing.
     * @param array  $args        Provided arguments
     * @param bool   $useRequired Set to true to show the required fn text if available instead of the documentation.
     * @return string
     */
    private function getArgMessage(string $method, string $name, array $args = [], $useRequired = false): string
    {
        $arg = $this->argDefinitions[$method][$name];
        $msg = '';
        $modifiers = [];

        if (isset($arg['type'])) {
            $modifiers[] = implode('|', $arg['type']);
        }

        if ($modifiers) {
            $msg .= '(' . implode('; ', $modifiers) . ')';
        }

        $msg = wordwrap("{$name}: {$msg}", 75, "\n  ");

        if ($useRequired && is_callable($arg['required'])) {
            $msg .= "\n\n  ";
            $msg .= str_replace("\n", "\n  ", call_user_func($arg['required'], $args));
        } elseif (isset($arg['doc'])) {
            $msg .= wordwrap("\n\n  {$arg['doc']}", 75, "\n  ");
        }

        return $msg;
    }

    /**
     * Throw when an invalid type is encountered.
     *
     * @param string $method
     * @param string $name     Name of the value being validated.
     * @param mixed  $provided The provided value.
     * @void
     * @throws IAE
     */
    private function invalidType(string $method, string $name, $provided)//: void
    {
        $expected = implode('|', $this->argDefinitions[$method][$name]['type']);

        throw new IAE(sprintf(
            'Invalid configuration value provided for "%s". Expected %s, but got %s%s%s',
            $name,
            $expected,
            Utils::describeType($provided),
            PHP_EOL.PHP_EOL,
            $this->getArgMessage($method, $name)
        ));
    }

    /**
     * Throws an exception for missing required arguments.
     *
     * @param string $method
     * @param array $args Passed in arguments.
     * @void
     * @throws IAE
     */
    private function throwRequired(string $method, array $args)//:void
    {
        $missing = [];

        foreach ($this->argDefinitions[$method] as $key => $a) {
            if (empty($a['required']) || isset($args[$a['position']])) {
                continue;
            }

            $missing[] = $this->getArgMessage($method, $key, $args, true);
        }

        throw new IAE(
            'Missing required call argument: '.
            PHP_EOL.PHP_EOL.
            implode(PHP_EOL.PHP_EOL, $missing)
        );
    }
}