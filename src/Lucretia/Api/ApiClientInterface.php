<?php

namespace Swigle\Lucretia\Api;

use stdClass;
use Swigle\Lucretia\LucretiaClientInterface;
use Swigle\Lucretia\Api\Collection\CollectionInterface;
use Swigle\Lucretia\Api\Collection\FilterInterface;

/**
 * Class ConnectorClient
 *
 *
 * @method stdClass get(array $query = [])
 * @method stdClass post(array $query = [], array $data = null)
 * @method stdClass put(array $query = [], array $data = null)
 *
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 10/09/2018 16:42
 */
interface ApiClientInterface extends LucretiaClientInterface
{
    /**
     * @return CollectionInterface
     */
    public function collection(FilterInterface $filter = null): CollectionInterface;

    /**
     * @return string
     */
    public function getApi(): string;
}