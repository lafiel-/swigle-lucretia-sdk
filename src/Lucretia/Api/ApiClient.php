<?php

namespace Swigle\Lucretia\Api;

use GuzzleHttp\ClientInterface as HttpClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use function GuzzleHttp\json_decode;
use stdClass;
use Swigle\Lucretia\Api\Collection\CollectionFactory;
use Swigle\Lucretia\Api\Collection\CollectionFactoryInterface;
use Swigle\Lucretia\Api\Collection\CollectionInterface;
use Swigle\Lucretia\Api\Collection\FilterInterface;
use Swigle\Lucretia\Exception\ConfigurationException;
use Swigle\Lucretia\Exception\NotMethodAllowedException;
use Swigle\Lucretia\LucretiaClient;

/**
 * Class ApiClient
 *
 * @method stdClass get(int $id)
 * @method stdClass post(array $data = null)
 * @method stdClass put(int $id, array $data = null)
 * @method stdClass delete(int $id)
 *
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 10/09/2018 16:42
 */
class ApiClient extends LucretiaClient implements ApiClientInterface
{
    /**
     * @var string
     */
    protected $api;

    /**
     * @var string[]
     */
    private $methods = [];

    /**
     * @var CollectionFactoryInterface
     */
    private $collectionFactory;

    /**
     * ApiClient constructor.
     * @param array $config
     * @param HttpClientInterface|null $client
     * @param CollectionFactoryInterface $collectionFactory
     * @throws GuzzleException
     */
    public function __construct(array $config = [], HttpClientInterface $client = null, CollectionFactoryInterface $collectionFactory = null)
    {
        parent::__construct($config, $client);

        $this->collectionFactory = $collectionFactory ?? new CollectionFactory();

        $this->api = $this->camel2id($config['api']);

        $this->fetchAllowedMethods();
    }

    /**
     * @inheritdoc
     * @return array
     */
    public static function getArguments(): array
    {
        $args = parent::getArguments();

        return $args + [
            'api' => [
                'type'    => 'value',
                'valid'   => ['string'],
                'doc'     => 'The lucretia API we\'re going to use',
            ],
        ];
    }

    /**
     * @return string
     */
    public function getApi(): string
    {
        return $this->api;
    }

    /**
     * @param FilterInterface|null $filter
     * @return CollectionInterface
     */
    public function collection(FilterInterface $filter = null): CollectionInterface
    {
        return $this->collectionFactory->create($this, $filter);
    }

    /**
     * @param string $method
     * @param array $args
     * @return stdClass|null
     * @throws GuzzleException
     * @throws ConfigurationException
     */
    public function __call(string $method, array $args): stdClass //@todo 7.1 null | ?stdClass
    {
        // force lower case method names
        $method = strtolower($method);

        if (!$this->isMethodAllowed($method)) {
            throw new NotMethodAllowedException(sprintf(
                'API "%s" does not support Method "%s"',
                $this->api,
                $method
            ));
        }

        $resolver = new ArgumentsResolver();
        $args = $resolver->resolve($method, $args);

        // array $query = []
        $id = isset($args['id']) ? $args['id'] : null;
        // array $data = null
        $data = isset($args['data']) ? $args['data'] : null;

        $options = [];

        // add the data to post, if any
        if ($data !== null) {
            $options[RequestOptions::JSON] = $data;
        }

        $uri = $this->api;

        if ($id !== null) {
            $uri .= '/'.$id;
        }

        $cmd = $this->getCommand($uri, [], $options);
        $cmd->setMethod(strtoupper($method));

        $response = $this->execute($cmd);

        $data = json_decode($response->getBody());

        return $data;
    }

    /**
     * Converts a CamelCase name into an ID in lowercase.
     * Words in the ID may be concatenated using the specified character (defaults to '-').
     * For example, 'PostTag' will be converted to 'post-tag'.
     * @param string $name the string to be converted
     * @param string $separator the character used to concatenate the words in the ID
     * @param bool|string $strict whether to insert a separator between two consecutive uppercase chars, defaults to false
     * @return string the resulting ID
     */
    protected function camel2id($name, $separator = '-', $strict = false)
    {
        $regex = $strict ? '/[A-Z]/' : '/(?<![A-Z])[A-Z]/';
        if ($separator === '_') {
            return strtolower(trim(preg_replace($regex, '_\0', $name), '_'));
        } else {
            return strtolower(trim(str_replace('_', $separator, preg_replace($regex, $separator . '\0', $name)), $separator));
        }
    }

    /**
     * @param string $method
     * @return bool
     */
    private function isMethodAllowed(string $method): bool
    {
        return in_array($method, $this->methods);
    }

    /**
     * @throws GuzzleException
     * @void
     */
    private function fetchAllowedMethods()//: void
    {
        $cmd = $this->getCommand($this->api);
        $cmd->setMethod('OPTIONS');

        $response = $this->execute($cmd);

        $allowHeaders = $response->getHeader('Allow');

        $this->methods = array_map('strtolower', array_map('trim', explode(',', array_shift($allowHeaders))));
    }
}
