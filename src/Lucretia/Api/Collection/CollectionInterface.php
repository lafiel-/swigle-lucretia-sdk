<?php

namespace Swigle\Lucretia\Api\Collection;

use Countable;
use GuzzleHttp\Exception\GuzzleException;
use Iterator;

/**
 * Class Collection
 *
 * @package Swigle\Lucretia\Api
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 11/09/2018 16:26
 */
interface CollectionInterface extends Iterator, Countable
{
    /**
     * @param int $perPage
     */
    public function setPerPage(int $perPage);
}