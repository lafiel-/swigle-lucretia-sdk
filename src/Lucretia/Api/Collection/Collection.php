<?php


namespace Swigle\Lucretia\Api\Collection;

use GuzzleHttp\Exception\GuzzleException;
use Swigle\Lucretia\Api\ApiClientInterface;
use Swigle\Lucretia\Exception\CanNotSetPerPageException;
use Swigle\Lucretia\Exception\CollectionHasChangedException;
use stdClass;


/**
 * Class Collection
 *
 * @package Swigle\Lucretia\Api
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 11/09/2018 16:26
 */
class Collection implements CollectionInterface
{
    /**
     * @var ApiClientInterface
     */
    protected $client;

    /**
     * @var FilterInterface
     */
    protected $filter;

    /**
     * @var int
     */
    private $position;

    /**
     * @var int
     */
    private $currentPage;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var string|null
     */
    private $dataHash;

    /**
     * @var int
     */
    private $totalCount;

    /**
     * @var int
     */
    private $perPage = 20;

    /**
     * @var bool
     */
    private $locked = false;

    /**
     * Collection constructor.
     * @param ApiClientInterface $client
     * @param FilterInterface|null $filter
     * @throws GuzzleException
     */
    public function __construct(ApiClientInterface $client, FilterInterface $filter = null)
    {
        $this->client = $client;
        $this->filter = $filter;
        $this->position = 0;
        $this->currentPage = 1;

        $this->loadHead();

        $this->loadPage();
    }

    /**
     * @inheritdoc
     * @return stdClass
     */
    public function current(): stdClass
    {
        return $this->data[$this->getDataPosition()];
    }

    /**
     * @inheritdoc
     * @void
     * @throws GuzzleException
     */
    public function next()//: void
    {
        $this->locked = true;
        ++$this->position;

        if ($this->currentPage !== $this->getPage()) {
            $this->loadPage();
        }
    }

    /**
     * @inheritdoc
     * @return int
     */
    public function key(): int
    {
        return $this->position;
    }

    /**
     * @inheritdoc
     * @return bool
     */
    public function valid(): bool
    {
        return (
            $this->position >= 0 &&
            $this->position < $this->totalCount &&
            isset($this->data[$this->getDataPosition()])
        );
    }

    /**
     * @inheritdoc
     * @void
     * @throws GuzzleException
     */
    public function rewind()//: void
    {
        $this->position = 0;
        $this->currentPage = 1;
        $this->locked = false;

        $this->loadPage();
    }

    /**
     * @inheritdoc
     * @return int
     */
    public function count(): int
    {
        return $this->totalCount;
    }

    /**
     * Set the number of items per page
     *
     * @nore the server determins the max items per page, if $perPage exeeds this amount the server will use the max
     * @param int $perPage
     * @throws GuzzleException
     */
    public function setPerPage(int $perPage)
    {
        if ($this->locked) {
            throw new CanNotSetPerPageException('It\'s not possible to change the items per page while the iterator is in use');
        }

        $this->perPage = $perPage;

        $this->loadPage(true);
    }

    /**
     * @throws GuzzleException
     */
    private function loadHead()
    {
        $headCmd = $this->client->getCommand($this->client->getApi(), $this->compileQuery());
        $headCmd->setMethod('HEAD');

        $headResponse = $this->client->execute($headCmd);

        $totalCountHeader = $headResponse->getHeader('X-Pagination-Total-Count');
        $dataSetHashHeader = $headResponse->getHeader('X-Lucretia-Data-Set-Hash');

        $this->totalCount = array_shift($totalCountHeader);
        $this->dataHash = array_shift($dataSetHashHeader);
    }

    /**
     * Load a new page of items
     *
     * @param bool $updatePerPage determins if the perPage should be updated from the server side
     * @throws GuzzleException
     */
    private function loadPage(bool $updatePerPage = false)
    {
        $api = $this->client->getApi();
        $pagedCmd = $this->client->getCommand($api, $this->compileQuery());
        $pagedCmd->setMethod('GET');

        $response = $this->client->execute($pagedCmd);

        $dataSetHashHeader = $response->getHeader('X-Lucretia-Data-Set-Hash');

        $t = array_shift($dataSetHashHeader);

        if ($this->dataHash != $t) {
            throw new CollectionHasChangedException(sprintf(
                'The collection has changed, you must restart your iteration for "%s"',
                $api
            ));
        }

        $currentPageHeader = $response->getHeader('X-Pagination-Current-Page');
        $this->currentPage = (int)array_shift($currentPageHeader);

        // Update the items perPage from the server side
        if ($updatePerPage) {
            $currentPageHeader = $response->getHeader('X-Pagination-Per-Page');
            $this->perPage = (int)array_shift($currentPageHeader);
        }

        $this->data = json_decode($response->getBody());
    }

    /**
     * Compiles the query parameters
     * @return array
     */
    private function compileQuery(): array
    {
        $query = [
            'per-page' => $this->perPage,
            'page' => $this->getPage(),
        ];

        if ($this->filter !== null) {
            $query['filter'] = $this->filter->build();
        }

        return $query;
    }

    /**
     * Calculate on which page we are based on the items per page and the current position
     * @return int
     */
    private function getPage(): int
    {
        return (int)floor($this->position / $this->perPage) + 1;
    }

    /**
     * Calculate the position within the current data batch.
     * @return int
     */
    private function getDataPosition(): int
    {
        return $this->position - (($this->getPage() - 1) * $this->perPage);
    }
}