<?php

namespace Swigle\Lucretia\Api\Collection;

/**
 * Interface FilterInterface
 *
 * @link https://www.yiiframework.com/doc/api/2.0/yii-data-datafilter
 * 
 * @package Swigle\Lucretia\Api\Collection
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 2019-04-12 14:14
 */
interface FilterInterface
{
    /**
     * Builds actual filter specification.
     *
     * @link https://www.yiiframework.com/doc/api/2.0/yii-data-datafilter
     *
     * @return array built actual filter value.
     */
    public function build(): array;
}
