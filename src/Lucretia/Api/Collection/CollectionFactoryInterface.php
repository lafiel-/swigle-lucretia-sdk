<?php

namespace Swigle\Lucretia\Api\Collection;

use Swigle\Lucretia\Api\ApiClientInterface;

/**
 * Class CollectionFactory
 *
 * @package Swigle\Lucretia\Api\Collection
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 14/09/2018 13:49
 */
interface CollectionFactoryInterface
{
    /**
     * Create a new CollectionInterface
     * @param ApiClientInterface $client
     * @param FilterInterface|null $filter
     * @return CollectionInterface
     */
    public function create(ApiClientInterface $client, FilterInterface $filter = null): CollectionInterface;
}