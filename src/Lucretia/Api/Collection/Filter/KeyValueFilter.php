<?php

namespace Swigle\Lucretia\Api\Collection\FIlter;

use Swigle\Lucretia\Api\Collection\FilterInterface;

/**
 * Class KeyValueFilter
 *
 * @package Swigle\Lucretia\Api\Collection\FIlter
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 2019-04-12 14:29
 */
final class KeyValueFilter implements FilterInterface
{
    private $keyValuePairs = [];

    /**
     * Add a key-value pair to the filter
     *
     * @param string $key
     * @param $value
     * @void
     */
    public function addPair(string $key, $value)//: void
    {
        $this->keyValuePairs[$key] = $value;
    }

    /**
     * @return array
     */
    public function build(): array
    {
        return $this->keyValuePairs;
    }
}
