<?php

namespace Swigle\Lucretia\Passthrough;

use Elao\Enum\ChoiceEnumTrait;
use Elao\Enum\ReadableEnum;

/**
 * Class FilterOperators
 *
 * @method static EQUAL(): FilterOperators
 * @method static LARGER_OR_EQUAL(): FilterOperators
 * @method static SMALLER_OR_EQUAL(): FilterOperators
 * @method static LARGER_THAN(): FilterOperators
 * @method static SMALLER_THAN(): FilterOperators
 * @method static LIKE(): FilterOperators
 * @method static NOT_EQUAL(): FilterOperators
 * @method static EMPTY(): FilterOperators
 * @method static NOT_EMPTY(): FilterOperators
 * @method static STARTS_WITH(): FilterOperators
 * @method static NOT_LIKE(): FilterOperators
 * @method static NOT_STARTS_WITH(): FilterOperators
 * @method static ENDS_WITH(): FilterOperators
 * @method static NOT_ENDS_WITH(): FilterOperators
 *
 * 'alias' constants because "like" is a bit ambiguous.
 * @method static CONTAINS(): FilterOperators
 * @method static NOT_CONTAINS(): FilterOperators
 *
 * @package Swigle\Lucretia\Passthrough
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 2019-04-24 13:26
 */
class FilterOperators extends ReadableEnum
{
    use ChoiceEnumTrait;

    const EQUAL             = 1;
    const LARGER_OR_EQUAL   = 2;
    const SMALLER_OR_EQUAL  = 3;
    const LARGER_THAN       = 4;
    const SMALLER_THAN      = 5;
    const LIKE              = 6;
    const NOT_EQUAL         = 7;
    const EMPTY             = 8;
    const NOT_EMPTY         = 9;
    const STARTS_WITH       = 19;
    const NOT_LIKE          = 11;
    const NOT_STARTS_WITH   = 12;
    const ENDS_WITH         = 13;
    const NOT_ENDS_WITH     = 4;

    // 'alias' constants because "like" is a bit ambiguous.
    const CONTAINS          = 6;
    const NOT_CONTAINS      = 11;

    /**
     * @return array
     */
    public static function choices(): array
    {
        return [
            self::EQUAL             => '==',
            self::LARGER_OR_EQUAL   => '>=',
            self::SMALLER_OR_EQUAL  => '<=',
            self::LARGER_THAN       => '>',
            self::SMALLER_THAN      => '<',
            self::LIKE              => 'Like',
            self::NOT_EQUAL         => '!=',
            self::EMPTY             => 'Empty',
            self::NOT_EMPTY         => 'Not empty',
            self::STARTS_WITH       => 'Starts with',
            self::NOT_LIKE          => 'Not like',
            self::NOT_STARTS_WITH   => 'Not starts with',
            self::ENDS_WITH         => 'Ends with',
            self::NOT_ENDS_WITH     => 'Not ends with',
            self::CONTAINS          => 'Contains',
            self::NOT_CONTAINS      => 'Not contains',
        ];
    }
}
