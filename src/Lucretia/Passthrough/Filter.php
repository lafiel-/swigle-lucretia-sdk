<?php

namespace Swigle\Lucretia\Passthrough;

/**
 * Class Filter
 *
 * @package Swigle\Lucretia\Passthrough
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 2019-04-24 11:10
 */
class Filter
{
    /**
     * @var string
     */
    public $filterFieldIds = '';

    /**
     * @var string
     */
    public $filterValues = '';

    /**
     * @var string
     */
    public $operatorTypes = '';

    /**
     * @var array
     */
    private $keyValuePairs = [];

    /**
     * @var array
     */
    private $operators     = [];

    /**
     * PHP magic method
     * @return string
     */
    public function __toString(): string
    {
        return http_build_query($this);
    }

    /**
     * Add a field to the filter
     * @param string $field
     * @param $value
     * @param FilterOperators $operator
     * @link https://github.com/rmuit/PracticalAfas/blob/b5352c159163d21ecdcb89766588ccf601894eba/src/Connection.php
     * @return Filter
     */
    public function addFilter(string $field, $value, FilterOperators $operator = null): self
    {
        if ($operator === null) {
            $operator = FilterOperators::LIKE();
        }

        $this->keyValuePairs[$field] = $value;
        $this->operators[$field]     = $operator->getValue();

        $this->compile();

        return $this;
    }

    /**
     * Remove a field from the filter
     * @param string $field
     */
    public function removeFilter(string $field): void
    {
        unset($this->keyValuePairs[$field]);
        unset($this->operators[$field]);

        $this->compile();
    }


    /**
     * Clear the filter
     */
    public function clear(): void
    {
        $this->keyValuePairs = [];
        $this->operators = [];

        $this->compile();
    }

    /**
     * get the filter as an array
     * @return array
     */
    public function toArray(): array
    {
        return [
            'filterfieldids' => $this->filterFieldIds,
            'filtervalues'   => $this->filterValues,
            'operatortypes'  => $this->operatorTypes,
        ];
    }

    /**
     * Compiles the query string fields
     */
    private function compile(): void
    {
        $this->filterFieldIds = implode(',', array_keys($this->keyValuePairs));
        $this->filterValues   = implode(',', array_values($this->keyValuePairs));
        $this->operatorTypes  = implode(',', array_values($this->operators));
    }
}
