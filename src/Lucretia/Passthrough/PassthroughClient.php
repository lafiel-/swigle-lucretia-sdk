<?php

namespace Swigle\Lucretia\Passthrough;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use InvalidArgumentException;
use LogicException;
use stdClass;
use Swigle\Lucretia\Exception\ConfigurationException;
use Swigle\Lucretia\LucretiaClient;

/**
 * Class PassthroughClient
 *
 *
 * @method stdClass get(string $path, array $query = [], array $data = null)
 * @method stdClass post(string $path, array $query = [], array $data = null)
 * @method stdClass put(string $path, array $query = [], array $data = null)
 *
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 10/09/2018 16:42
 */
class PassthroughClient extends LucretiaClient
{
    /**
     * @param string $method
     * @param array $args
     * @return mixed|null
     * @throws GuzzleException
     * @throws ConfigurationException
     */
    public function __call(string $method, array $args)//: \stdClass @todo 7.1 null | \stdClass
    {
        if (count($args) < 1 || count($args) > 3) {
            throw new LogicException('must takes 1 to 3 arguments');
        }
        // string $path
        $path = $args[0];
        // array $query = []
        $query = isset($args[1]) ? $args[1] : [];
        // array $data = null
        $data = isset($args[2]) ? $args[2] : null;

        if (!is_string($path)) {
            throw new InvalidArgumentException('The first argument ($path) must be a string');
        }

        if (!is_array($query) && $query !== null) {
            throw new InvalidArgumentException('The second argument ($query) must be a array');
        }

        if (!is_array($data) && $data !== null) {
            throw new InvalidArgumentException('The third argument ($data) must be a array');
        }

        if (!in_array(strtolower($method), ['get', 'post', 'put'])) {
            throw new LogicException('Invalid passthrough method');
        }

        // defining variables
        $options = [
            'headers' => $this->buildHeaders(),
        ];

        // add the data to post, if any
        if (in_array(strtolower($method), ['post', 'put']) && $data !== null) {
            $options[RequestOptions::JSON] = $data;
        }

        $uri = $this->buildUri($path, 'passthrough')
            ->withQuery(http_build_query($query))
        ;

        $response = $this->client->request(
            $method,
            $uri,
            $options
        );

        $this->statusCode = $response->getStatusCode();

        if ((string) $response->getBody() == '' && $this->statusCode >= 200 && $this->statusCode < 300) {
            return null;
        }

        return \GuzzleHttp\json_decode($response->getBody());
    }
}
