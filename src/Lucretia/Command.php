<?php


namespace Swigle\Lucretia;

use Psr\Http\Message\UriInterface;

/**
 * Interface Command
 *
 * @package Swigle\Lucretia
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 10/09/2018 16:44
 */
class Command implements CommandInterface
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var UriInterface
     */
    private $uri;

    /**
     * @var array
     */
    private $options;

    /**
     * Command constructor.
     * @param UriInterface $uri
     * @param array $options
     */
    public function __construct(UriInterface $uri, array $options = [])
    {
        $this->uri = $uri;
        $this->options = $options;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @void
     */
    public function setMethod(string $method)//: void
    {
        $this->method = strtoupper($method);
    }

    /**
     * @return bool
     */
    public function hasMethod(): bool
    {
        return $this->method !== null;
    }

    /**
     * @return UriInterface
     */
    public function getUri(): UriInterface
    {
        return $this->uri;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}
