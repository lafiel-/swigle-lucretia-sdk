<?php

namespace Swigle\Lucretia;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

/**
 * Represents an Lucretia client.
 */
interface LucretiaClientInterface
{
    /**
     * Creates and executes a command for an operation by name.
     *
     * @param string $name      Name of the command to execute.
     * @param array  $arguments Arguments to pass to the getCommand method.
     *
     * @return mixed
     * @ return ResultInterface
     * @throws Exception
     */
    public function __call(string $name, array $arguments);

    /**
     * Gets the status code of the call
     * @see LucretiaClientInterface::__call
     * @return int
     */
    public function getStatusCode(): int;

    /**
     * Execute a single command.
     *
     * @param CommandInterface $command Command to execute
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function execute(CommandInterface $command): ResponseInterface;

    /**
     * Create a command for an operation name.
     *
     * Special keys may be set on the command to control how it behaves.
     *
     * @param string $name Name of the api to use in the command
     * @param array $query Query to pass to the command
     * @param array $options Optionn to pass to the command
     * @return CommandInterface
     */
    public function getCommand(string $name, array $query = [], array $options = []): CommandInterface;
}
