<?php

namespace Swigle\Lucretia;

use Closure as PhpClosure;
use InvalidArgumentException as IAE;

/**
 * Class ClientResolver
 * @internal Resolves a hash of client arguments to construct a client.
 * @package Swigle\Lucretia
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 11/09/2018 12:00
 */
class ClientResolver
{
    /** @var array */
    private $argDefinitions;

    /** @var array Map of types to a corresponding function */
    private static $typeMap = [
        'resource' => 'is_resource',
        'callable' => 'is_callable',
        'int'      => 'is_int',
        'bool'     => 'is_bool',
        'string'   => 'is_string',
        'object'   => 'is_object',
        'array'    => 'is_array',
    ];

    private static $defaultArgs = [
        'token' => [
            'type'     => 'value',
            'valid'    => ['string'],
            'doc'      => 'The API token key to utilize. This value will be supplied by default when using one of the SDK clients.',
            'required' => true,
            'internal' => true
        ],
        'company' => [
            'type'     => 'value',
            'valid'    => ['string'],
            'doc'      => 'The company name of the service to utilize. This value will be supplied by default when using one of the SDK clients (e.g., Aws\\S3\\S3Client).',
            'required' => true,
            'internal' => true
        ],
        'environment' => [
            'type'     => 'value',
            'valid'    => ['string'],
            'doc'      => 'The environment name of the service to utilize. This value will be supplied by default when using one of the SDK clients (e.g., Aws\\S3\\S3Client).',
            'required' => true,
            'internal' => true
        ],
        'endpoint' => [
            'type'  => 'value',
            'valid' => ['string'],
            'doc'   => 'The full URI of the webservice. This is only required when connecting to a custom endpoint.',
            'fn'    => [__CLASS__, '_apply_endpoint'],
        ],

        'scheme' => [
            'type'     => 'value',
            'valid'    => ['string'],
            'default'  => 'https',
            'doc'      => 'URI scheme to use when connecting connect. The SDK will utilize "https" endpoints (i.e., utilize SSL/TLS connections) by default. You can attempt to connect to a service over an unencrypted "http" endpoint by setting ``scheme`` to "http".',
        ],
        'user_agent' => [
            'type'     => 'value',
            'valid'    => ['string', 'array'],
            'doc'      => 'Provide a string or array of strings to send in the User-Agent header.',
            'fn'       => [__CLASS__, '_apply_user_agent'],
            'default'  => [],
        ],
    ];

    /**
     * Gets an array of default client arguments, each argument containing a
     * hash of the following:
     *
     * - type: (string, required) option type described as follows:
     *   - value: The default option type.
     *   - config: The provided value is made available in the client's
     *     getConfig() method.
     * - valid: (array, required) Valid PHP types or class names. Note: null
     *   is not an allowed type.
     * - required: (bool, callable) Whether or not the argument is required.
     *   Provide a function that accepts an array of arguments and returns a
     *   string to provide a custom error message.
     * - default: (mixed) The default value of the argument if not provided. If
     *   a function is provided, then it will be invoked to provide a default
     *   value. The function is provided the array of options and is expected
     *   to return the default value of the option. The default value can be a
     *   closure and can not be a callable string that is not  part of the
     *   defaultArgs array.
     * - doc: (string) The argument documentation string.
     * - fn: (callable) Function used to apply the argument. The function
     *   accepts the provided value, array of arguments by reference, and an
     *   event emitter.
     *
     * Note: Order is honored and important when applying arguments.
     *
     * @return array
     */
    public static function getDefaultArguments(): array
    {
        return self::$defaultArgs;
    }

    /**
     * @param array $argDefinitions Client arguments.
     */
    public function __construct(array $argDefinitions)
    {
        $this->argDefinitions = $argDefinitions;
    }

    /**
     * Resolves client configuration options and attached event listeners.
     * Check for missing keys in passed arguments
     *
     * @param array       $args Provided constructor arguments.
     *
     * @return array Returns the array of provided options.
     * @throws \InvalidArgumentException
     * @see LucretiaClientt::__construct for a list of available options.
     */
    public function resolve(array $args): array
    {
        $args['config'] = [];
        foreach ($this->argDefinitions as $key => $a) {
            // Add defaults, validate required values, and skip if not set.
            if (!isset($args[$key])) {
                if (isset($a['default'])) {
                    // Merge defaults in when not present.
                    if (is_callable($a['default'])
                        && (
                            is_array($a['default'])
                            || $a['default'] instanceof PhpClosure
                        )
                    ) {
                        $args[$key] = $a['default']($args);
                    } else {
                        $args[$key] = $a['default'];
                    }
                } elseif (empty($a['required'])) {
                    continue;
                } else {
                    $this->throwRequired($args);
                }
            }

            // Validate the types against the provided value.
            foreach ($a['valid'] as $check) {
                if (isset(self::$typeMap[$check])) {
                    $fn = self::$typeMap[$check];
                    if ($fn($args[$key])) {
                        goto is_valid;
                    }
                } elseif ($args[$key] instanceof $check) {
                    goto is_valid;
                }
            }

            $this->invalidType($key, $args[$key]);

            // Apply the value
            is_valid:
            if (isset($a['fn'])) {
                $a['fn']($args[$key], $args);
            }

            if ($a['type'] === 'config') {
                $args['config'][$key] = $args[$key];
            }
        }

        return $args;
    }

    /**
     * Creates a verbose error message for an invalid argument.
     *
     * @param string $name        Name of the argument that is missing.
     * @param array  $args        Provided arguments
     * @param bool   $useRequired Set to true to show the required fn text if
     *                            available instead of the documentation.
     * @return string
     */
    private function getArgMessage($name, $args = [], $useRequired = false): string
    {
        $arg = $this->argDefinitions[$name];
        $msg = '';
        $modifiers = [];
        if (isset($arg['valid'])) {
            $modifiers[] = implode('|', $arg['valid']);
        }
        if (isset($arg['choice'])) {
            $modifiers[] = 'One of ' . implode(', ', $arg['choice']);
        }
        if ($modifiers) {
            $msg .= '(' . implode('; ', $modifiers) . ')';
        }
        $msg = wordwrap("{$name}: {$msg}", 75, "\n  ");

        if ($useRequired && is_callable($arg['required'])) {
            $msg .= "\n\n  ";
            $msg .= str_replace("\n", "\n  ", call_user_func($arg['required'], $args));
        } elseif (isset($arg['doc'])) {
            $msg .= wordwrap("\n\n  {$arg['doc']}", 75, "\n  ");
        }

        return $msg;
    }

    /**
     * Throw when an invalid type is encountered.
     *
     * @param string $name     Name of the value being validated.
     * @param mixed  $provided The provided value.
     * @void
     * @throws IAE
     */
    private function invalidType(string $name, $provided)//: void
    {
        $expected = implode('|', $this->argDefinitions[$name]['valid']);

        throw new IAE(sprintf(
            'Invalid configuration value provided for "%s". Expected %s, but got %s%s%s',
            $name,
            $expected,
            Utils::describeType($provided),
            PHP_EOL.PHP_EOL,
            $this->getArgMessage($name)
        ));
    }

    /**
     * Throws an exception for missing required arguments.
     *
     * @param array $args Passed in arguments.
     * @void
     * @throws IAE
     */
    private function throwRequired(array $args)//:void
    {
        $missing = [];

        foreach ($this->argDefinitions as $k => $a) {
            if (empty($a['required'])
                || isset($a['default'])
                || isset($args[$k])
            ) {
                continue;
            }
            $missing[] = $this->getArgMessage($k, $args, true);
        }

        throw new IAE(
            'Missing required client configuration options: '.
            PHP_EOL.PHP_EOL.
            implode(PHP_EOL.PHP_EOL, $missing)
        );
    }

    /**
     * @param string|array $value
     * @param array &$args
     * @void
     */
    public static function _apply_user_agent($value, array &$args)//: void
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        $value = array_map('strval', $value);

        if (defined('HHVM_VERSION')) {
            array_unshift($value, 'HHVM/' . HHVM_VERSION);
        }
        array_unshift($value, 'lucretia-sdk-php/' . Sdk::VERSION);
        $args['user_agent'] = $value;
    }

    /**
     * @param string $value
     * @param array $args
     * @void
     */
    public static function _apply_endpoint(string $value, array &$args)//: void
    {
        $parts = parse_url($value);
        if (empty($parts['scheme']) || empty($parts['host'])) {
            throw new IAE(
                'Endpoints must be full URIs and include a scheme and host'
            );
        }

        $args['endpoint'] = $value;
    }
}
