<?php

namespace Swigle\Lucretia\Exception;

use RuntimeException;
use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

/**
 * Class CollectionHasChangedException
 *
 * This Exception is thrown when the dataset has changed on the lucretia platform,
 * it's based on the dataset creation timestamp and it's size
 *
 * @package Swigle\Lucretia\Exception
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 11/09/2018 17:02
 */
class CollectionHasChangedException extends ServerException
{
    /**
     * Get the amount of seconds which we need to wait before restarting our collection
     *
     * @return float
     */
    public function getRetryAfter(): float
    {
        $prev = $this->getPrevious();

        $retryAfter = 0.0;

        if ($prev !== null) {

            if (!($prev instanceof GuzzleRequestException)) {
                throw new RuntimeException(
                    'DAFUQ, at this point we should always have a prev exception which is a GuzzleRequestException'
                );
            }

            $response = $prev->getResponse();

            if ($response === null) {
                throw new RuntimeException('DAFUQ, at this point we should always have a response');
            }

            $header = $response->getHeader('retry-after');

            if (count($header) !== 1) {
                throw new RuntimeException('DAFUQ, at this point we should always have a header');
            }

            $retryAfter = $header[0];
        }

        return $retryAfter;
    }
}