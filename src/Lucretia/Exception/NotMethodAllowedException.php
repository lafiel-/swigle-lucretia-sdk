<?php

namespace Swigle\Lucretia\Exception;

/**
 * Class NotMethodAllowedException
 *
 * @package Swigle\Lucretia\Exception
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 20/11/2017 10:22
 */
class NotMethodAllowedException extends LucretiaException
{

}