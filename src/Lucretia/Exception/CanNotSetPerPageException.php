<?php

namespace Swigle\Lucretia\Exception;

use LogicException;

/**
 * Class CanNotSetPerPageException
 *
 * This exception is thrown when a developer tries to set the page size, but the iterator is already running
 *
 * @package Swigle\Lucretia\Exception
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 12/09/2018 10:13
 */
class CanNotSetPerPageException extends LogicException
{

}