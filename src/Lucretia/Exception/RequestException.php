<?php

namespace Swigle\Lucretia\Exception;

use GuzzleHttp\Exception\RequestException as BaseRequestException;

/**
 * Class RequestException
 *
 * @package Swigle\Lucretia\Exception
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 13/09/2018 11:01
 */
class RequestException extends LucretiaException
{
    /**
     * Factory method to create a new exception with a normalized error message
     *
     * @param BaseRequestException $exception
     *
     * @return self
     */
    public static function create(BaseRequestException $exception)
    {
        $exceptionClass = self::class;

        $response = $exception->getResponse();

        $level = (int) floor($response->getStatusCode() / 100);
        if ($response->getStatusCode() === 423) {
            $label = 'Collection error, you must restart your iteration';
            $exceptionClass = CollectionHasChangedException::class;
        } elseif ($level === 4) {
            $label = 'Client error';
            $exceptionClass = ClientException::class;
        } elseif ($level === 5) {
            $label = 'Server error';
            $exceptionClass = ServerException::class;
        } else {
            $label = 'Unsuccessful request';
        }

        $responseBody = (string)$response->getBody();

        if ($responseBody !== '') {
            $responseData = \GuzzleHttp\json_decode($response->getBody());

            return new $exceptionClass($label . ' - ' . $responseData->message, $responseData->code, $exception);
        } else {
            return new $exceptionClass($label . ' - ' . $response->getReasonPhrase(), $response->getStatusCode(), $exception);
        }
    }
}