<?php


namespace Swigle\Lucretia\Exception;

/**
 * Class InvalidCommandException
 *
 * @package Swigle\Lucretia\Exception
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 13/09/2018 11:16
 */
class InvalidCommandException extends LucretiaException
{

}