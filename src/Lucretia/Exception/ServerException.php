<?php

namespace Swigle\Lucretia\Exception;

/**
 * Class ServerException
 *
 * @package Swigle\Lucretia\Exception
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 2019-05-28 11:26
 */
class ServerException extends RequestException
{
}