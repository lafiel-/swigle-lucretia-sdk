<?php

namespace Swigle\Lucretia\Exception;

use RuntimeException;

/**
 * Class ConfigurationException
 *
 * @package Swigle\Lucretia\Exception
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 11/09/2018 10:41
 */
class ConfigurationException extends RuntimeException
{

}