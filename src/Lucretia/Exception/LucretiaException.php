<?php

namespace Swigle\Lucretia\Exception;

use RuntimeException;

/**
 * Class LucretiaException
 *
 * Exception base
 *
 * @package Swigle\Lucretia\Exception
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 13/09/2018 11:05
 */
class LucretiaException extends RuntimeException
{

}