<?php

namespace Swigle\Lucretia\Exception;

/**
 * Class ClientException
 *
 * @package Swigle\Lucretia\Exception
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 2019-05-28 11:26
 */
class ClientException extends RequestException
{
}