<?php

namespace Swigle\Lucretia;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\ClientInterface as HttpClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException as BaseRequestException;
use GuzzleHttp\Psr7;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Swigle\Lucretia\Exception\ConfigurationException;
use Swigle\Lucretia\Exception\InvalidCommandException;
use Swigle\Lucretia\Exception\LucretiaException;
use Swigle\Lucretia\Exception\RequestException;


/**
 * Class LucretiaClient
 * This is the base class for the actual Clients (Api/Passthrough)
 *
 * @package Swigle\Lucretia
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 10/09/2018 16:29
 */
abstract class LucretiaClient implements LucretiaClientInterface
{
    /**
     * @var int
     */
    protected $statusCode;

    /**
     * @var HttpClientInterface
     */
    protected $client;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var string
     */
    private $company;

    /**
     * @var string
     */
    private $environment;

    /**
     * @var string
     */
    private $userAgent;

    /**
     * Client constructor.
     * @param array $args
     * @param HttpClientInterface|null $client
     */
    public function __construct(array $args = [], HttpClientInterface $client = null)
    {
        $resolver = new ClientResolver(static::getArguments());
        $config = $resolver->resolve($args);

        $this->token = $config['token'];
        $this->endpoint = $config['endpoint'];
        $this->company = $config['company'];
        $this->environment = $config['environment'];

        $this->userAgent = $config['user_agent'];

        $this->client = $client ?? new HttpClient();
    }

    /**
     * Get an array of client constructor arguments used by the client.
     *
     * @return array
     */
    public static function getArguments(): array
    {
        return ClientResolver::getDefaultArguments();
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param CommandInterface $command
     * @return ResponseInterface
     * @throws LucretiaException
     * @throws GuzzleException
     */
    public function execute(CommandInterface $command): ResponseInterface
    {
        if (!$command->hasMethod()) {
            throw new InvalidCommandException('The given command must have a method');
        }

        try {
            $response = $this->client->request(
                $command->getMethod(),
                $command->getUri(),
                $command->getOptions()
            );
        } catch (BaseRequestException $requestException) {
            $response = $requestException->getResponse();

            // Using a factory to convert the guzzle exception into a lucretia exception
            throw RequestException::create($requestException);
        } finally {
            $this->statusCode = $response->getStatusCode();
        }

        return $response;
    }

    /**
     * @param string $name
     * @param array $query
     * @param array $options
     * @return CommandInterface
     */
    public function getCommand(string $name, array $query = [], array $options = []): CommandInterface
    {
        if (isset($options['headers'])) {
            $options['headers'] = array_merge($options['headers'], $this->buildHeaders());
        } else {
            $options['headers'] = $this->buildHeaders();
        }

        $uri = $this
            ->buildUri($name)
            ->withQuery(http_build_query($query)) // todo move this to the execute method for more flexibility
        ;

        return new Command($uri, $options);
    }

    /**
     * @param string $uri
     * @param string $prefix
     * @return UriInterface
     * @throws ConfigurationException when the participant number is not set
     */
    protected function buildUri(string $uri, string $prefix = 'api'): UriInterface
    {
        $uri = Psr7\uri_for($uri);

        $endPoint = sprintf(
            '%s/%s/%s/%s/',
            $this->endpoint,
            $prefix,
            $this->company,
            $this->environment
        );

        return Psr7\UriResolver::resolve(Psr7\uri_for($endPoint), $uri);
    }

    /**
     * @return array
     * @throws ConfigurationException when the api key is not set
     */
    protected function buildHeaders() : array
    {
        return [
            'Accept'        => 'application/json',
            'Content-Type'  => 'application/json',
            'Authorization' => 'Bearer '.$this->token,
            'User-Agent'    => $this->userAgent,
        ];
    }
}