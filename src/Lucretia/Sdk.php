<?php

namespace Swigle\Lucretia;

use BadMethodCallException;
use InvalidArgumentException;
use Swigle\Lucretia\Api\ApiClient;
use Swigle\Lucretia\Country\CountryClient;
use Swigle\Lucretia\Passthrough\PassthroughClient;

/**
 * Class Sdk
 *
 * @method PassthroughClient createPassthrough(array $args = [])
 * @method ApiClient createApi(array $args = [])
 * @method CountryClient createCountry(array $args = [])
 *
 * @package Swigle\Lucretia\SDK
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 10/09/2018 16:25
 */
class Sdk
{
    const VERSION = '2.2.0';

    /**
     * @var array Arguments for creating clients
     */
    private $args = [
        'endpoint' => 'http://app.lucretia.io/lucretia',
    ];

    /**
     * Constructs a new SDK object with an associative array of default
     * client settings.
     *
     * @param array $args
     *
     * @throws InvalidArgumentException
     * @see LucretiaClient::__construct for a list of available options.
     */
    public function __construct(array $args = [])
    {
        $this->args = $args;
    }

    /**
     * @param $name
     * @param array $args
     * @return LucretiaClientInterface
     */
    public function __call($name, array $args)
    {
        $args = isset($args[0]) ? $args[0] : [];

        if (strpos($name, 'create') === 0) {
            return $this->createClient(substr($name, 6), $args);
        }

        throw new BadMethodCallException("Unknown method: {$name}.");
    }

    /**
     * Get a client by name using an array of constructor options.
     *
     * @param string $name Service name or namespace (e.g., DynamoDb, s3).
     * @param array  $args Arguments to configure the client.
     *
     * @return LucretiaClientInterface
     * @throws InvalidArgumentException if any required options are missing or
     *                                   the service is not supported.
     * @see LucretiaClient::__construct for a list of available options for args.
     */
    public function createClient($name, array $args = [])
    {
        // Instantiate the client class.
        $client = __NAMESPACE__."\\{$name}\\{$name}Client";
        return new $client($this->mergeArgs($name, $args));
    }

    /**
     * @param string $name
     * @param array $args
     * @return array
     */
    private function mergeArgs(string $name, array $args = []): array
    {
        // Merge provided args with stored, service-specific args.
        if (isset($this->args[$name])) {
            $args += $this->args[$name];
        }

        return $args + $this->args;
    }
}