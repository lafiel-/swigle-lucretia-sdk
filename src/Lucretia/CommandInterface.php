<?php

namespace Swigle\Lucretia;

use Psr\Http\Message\UriInterface;

/**
 * Interface CommandInterface
 *
 * @package Swigle\Lucretia
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 10/09/2018 16:44
 */
interface CommandInterface
{
    /**
     * @return bool
     */
    public function hasMethod(): bool;

    /**
     * @return string
     */
    public function getMethod(): string;

    /**
     * @param string $method
     * @void
     */
    public function setMethod(string $method);//: void;

    /**
     * @return UriInterface
     */
    public function getUri(): UriInterface;

    /**
     * @return array
     */
    public function getOptions(): array;
}