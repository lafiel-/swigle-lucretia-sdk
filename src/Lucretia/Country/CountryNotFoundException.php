<?php

namespace Swigle\Lucretia\Country;

use Swigle\Lucretia\Exception\LucretiaException;

/**
 * Class CountryNotFoundException
 *
 * @package Swigle\Lucretia\Country
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 19/09/2018 17:16
 */
class CountryNotFoundException extends LucretiaException
{
}