<?php


namespace Swigle\Lucretia\Country;

use Swigle\Lucretia\ClientResolver;

/**
 * Class CountryClient
 *
 * @package Swigle\Lucretia\Country
 * @author Remko van Bezooijen <remko@swigle.com>
 * @since 19/09/2018 16:56
 */
class CountryClient
{
    /**
     * @var string[][]
     */
    private $countries = [];

    /**
     * CountryClient constructor.
     * @param array $args
     */
    public function __construct(array $args = [])
    {
        $resolver = new ClientResolver(static::getArguments());
        $config = $resolver->resolve($args);

        $this->loadCountries($config);
    }

    /**
     * Get an array of client constructor arguments used by the client.
     *
     * @return array
     */
    public static function getArguments(): array
    {
        return [
            'source' => [
                'type'    => 'value',
                'valid'   => ['string'],
                'doc'     => 'The default countries list, this requires a absolute path',
                'default' => __DIR__.'/countries.json',
            ],
        ];
    }

    /**
     * @param string $isoAlpha2
     * @return string
     */
    public function alpha2toProfit(string $isoAlpha2): string
    {
        if (!isset($this->countries['a2'][$isoAlpha2])) {
            throw new CountryNotFoundException(sprintf(
                'The Iso Alpha 2 code "%s" could not be translated',
                $isoAlpha2
            ));
        }

        return $this->countries['a2'][$isoAlpha2];
    }

    /**
     * @param string $isoAlpha3
     * @return string
     */
    public function alpha3toProfit(string $isoAlpha3): string
    {
        if (!isset($this->countries['a3'][$isoAlpha3])) {
            throw new CountryNotFoundException(sprintf(
                'The Iso Alpha 3 code "%s" could not be translated',
                $isoAlpha3
            ));
        }

        return $this->countries['a3'][$isoAlpha3];
    }

    /**
     * @param array $config
     * @void
     */
    private function loadCountries(array $config = [])//: void
    {
        $countries = json_decode(file_get_contents($config['source']), true);

        foreach ($countries as $country) {
            $this->countries['a2'][$country['ISO_Alpha-2']] = $country['Code_land'];
            $this->countries['a3'][$country['ISO_Alpha-3']] = $country['Code_land'];
        }
    }
}
