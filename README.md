# README #

## What is this repository for? ##

The Swigle Lucretia SDK library allows one to connect to Lucretia.
With this we can retrieve data from and submit data to lucretia for processing and forwarding to profit.

# DIRECTORY STRUCTURE

      .         contains the composer files, git files and this ReadMe file
      build/    contains the test results
      src/      contains the iibrary classes
      tests/    contains the iibrary test cases


# REQUIREMENTS

The minimum requirement by this library is PHP 7.0.0.
All secondary requirements are loaded by composer.

# INSTALLATION

### Install via Composer

If you do not have toran added to your composer.json , you may install it by following the instructions
at [https://toran-proxy.swigle.com/](https://toran-proxy.swigle.com/).

You can then install this package using the following command:

~~~
composer require swigle/lucretia-sdk
~~~

# Usage

The Clients can be created in multiple ways, but through the sdk (factory) is recommended.
```
$args = [
    'token' => 'test',
    'company' => 'company',
    'environment' => 'environment'

    'endpoint' => 'https://app.lucretia.io/lucretia',
];
$sdk = new Sdk($args);

$apiClient = $sdk->createApi($args+[
    'api' => '<name of api>'
]);
```

Getting data from an API
```
// single object
$object = $apiClient->get($id);

// $collection of objects over which you can iterate
$collection = $apiClient->collection();
```

Filtering the colletion with `KeyValueFilter`
`KeyValueFilter` is a simple key-value filter where you can add kv pair which wil then be passed on to the restful api
```
$filter = new KeyValueFilter();
$filter->addPair('sku', '00100-000000-0010');

// $collection of objects over which you can iterate
$collection = $apiClient->collection($filter);
```


Create a new entity
```
$apiClient->post($data);
```

Update a specific entity
```
$apiClient->put($id, $data);
```

Delete an entity
```
$apiClient->delete($id);
```

Converting Alpha 2/4 ISO country codes to profit country codes
```
$countryClient = $sdk->createCountry($args);
$profitCountryCode = $countryClient->alpha2toProfit('DE')
$profitCountryCode = $countryClient->alpha3toProfit('DEU')
```